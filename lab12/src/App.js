import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import AlbumList from './AlbumList';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
        <Container>
            <Row className="justify-content-center">
                <Col >
                    <AlbumList />
                </Col>
            </Row>
        </Container>
    </div>
  );
}

export default App;
