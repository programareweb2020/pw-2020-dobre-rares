import React, { useState} from 'react'
import axios from 'axios';
import PaginationList from './Pagination';


export default function AlbumList() {
    const [ albums, setAlbums ] = useState([]);
    const [requested, updaterequested] = useState(false);
    
    if (!requested) {
        updaterequested(true);
        axios.get('https://jsonplaceholder.typicode.com/albums')
        .then(response => {
            setAlbums(response.data);
        }).catch(error => {
            console.log(error)
        });
    }

    console.log(albums)

    return (
        <div>
            { albums === null ?
                <div>
                    <span>Loading...</span>
                </div>
                :
                <div>
                    <PaginationList albums={albums} />
                </div>
            }
        </div>
    )
}

