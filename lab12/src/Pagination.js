import React, { useState } from 'react';
import Album from './Album';
import Pagination from 'react-bootstrap/Pagination';


export default function PaginationList ({albums}) {

    const [ active, setActive ] = useState(1);
    const items = []
    
    for (let i = 1; i < albums.length / 10; i++)
        items.push(
            <Pagination.Item key={i} active={i === active}>
                {i}
            </Pagination.Item>
        )

    function pageChange (event) {
        setActive(parseInt(event.target.text));
    }

    return (
        <div>
        {
            albums.slice(active * 10, (active + 1) * 10).map(album => <Album key={album.id} album={album} />)
        }

        <Pagination onClick={pageChange}>
            {items}
        </Pagination>
        </div>
    )
}
