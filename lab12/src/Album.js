import React, { useState} from 'react';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Carousel from './Carousel';


export default function Album ({ album }){
    const [ carouselView, setCarouselView] = useState(false)
    const [ user, setUser ] = useState([]);
    const [requested, updaterequested] = useState(false);

    if (!requested) {
        updaterequested(true);
        axios.get(`https://jsonplaceholder.typicode.com/users/${album.userId}`)
        .then(response => {
            setUser(response.data);
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        <div>
        { carouselView ?
            <Carousel album={album} />
            :
            <Card onClick={() => {setCarouselView(true)}}>
                <Card.Body>
                    <Card.Title>{album.title}</Card.Title>
                    {user !== null && <Card.Text>by {user.username} aka {user.name}</Card.Text>}
                </Card.Body>
            </Card>
            }
        </div>
    )
}
