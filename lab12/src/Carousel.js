import React, { useState} from 'react';
import Carousel from 'react-bootstrap/Carousel';
import axios from 'axios';

export default function CarouselAlbum ({ album }) {
    const [ photos, setPhotos ] = useState([]);
    const [requested, updaterequested] = useState(false);

    if (!requested) {
        updaterequested(true);
        axios.get(`https://jsonplaceholder.typicode.com/albums/${album.id}/photos`)
        .then(response => {
            setPhotos(response.data);
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        <div>
            {
                photos === null ? <p>Loading...</p> :
                    <Carousel>
                        {photos.map(photo => (
                            <Carousel.Item key={photo.id}>
                                <img
                                    className="d-block w-100"
                                    src={`${photo.thumbnailUrl}`}
                                    alt="carousel"
                                />
                                <Carousel.Caption>
                                    <span>{photo.title}</span>
                                </Carousel.Caption>
                            </Carousel.Item>
                        ))}
                    </Carousel>
            }
        </div>
    )
}
