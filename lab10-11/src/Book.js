import React, { useState } from "react";
import axios from "axios";

function Book(props) {
  const { id } = props.match.params;
  const [book, setBookData] = useState({});
  const token = localStorage.getItem("token");
  const [requested, updaterequested] = useState(false);

  if (!requested) {
    updaterequested(true);
    axios
      .get(`http://localhost:3000/books/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(JSON.stringify(response));
        const jsonResponse = JSON.parse(JSON.stringify(response));
        setBookData(jsonResponse.data);
        console.log(JSON.parse(JSON.stringify(response)));
      })
      .catch((error) => console.error(error));
  }

  return (
    <div className="container-book">
      <div>Book ID: {book.id}</div>
      <div>Book name: {book.name}</div>
      <div>Book author firstname: {book.author.firstName}</div>
      <div>Book author lasttname: {book.author.lastName}</div>
      <ul>
        {book.genres.map((genre) => (
          <li key={genre}>{genre}</li>
        ))}
      </ul>
    </div>
  );
}
export default Book;
