import React from "react";
import axios from "axios";

class Login extends React.Component {
  state = {
    username: "",
    password: "",
  };

  handleInput = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  handleLogin = () => {
    console.log(this.state);
    axios
      .post("http://localhost:3000/login", {
        username: this.state.username,
        password: this.state.password,
      })
      .then((response) => {
        this.handleSuccess(response.data);
        console.log(response.data);
      })
      .catch((error) => console.error(error));
  };

  handleRegister = () => {
    console.log(this.state);
    axios
      .post("http://localhost:3000/register", {
        username: this.state.username,
        password: this.state.password,
      })
      .then((response) => {
        this.handleRegisterSuccess(response.data);
        console.log(response.data);
      })
      .catch((error) => console.error(error));
  };

  handleSuccess = (response) => {
    localStorage.setItem("token", response);
    console.log(localStorage.getItem("token"));
  };

  handleRegisterSuccess = (response) => {
    console.log(response);
  };

  render() {
    return (
      <section className="login-container">
        <h1>Login</h1>
        <span>Come to the awesome world!</span>
        <br></br>
        <label>Username</label>
        <input
          type="text"
          className="login-input"
          name="username"
          value={this.state.username}
          onChange={this.handleInput}
        />
        <label>Password</label>
        <input
          type="password"
          name="password"
          className="login-input"
          value={this.state.password}
          onChange={this.handleInput}
        />
        <button className="button" type="button" onClick={this.handleLogin}>
          Login
        </button>
        <button className="button" type="button" onClick={this.handleRegister}>
          Register
        </button>
      </section>
    );
  }
}
export default Login;
