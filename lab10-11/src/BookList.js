import React from "react";
import axios from "axios";
import { useState } from "react";
import { Link } from "react-router-dom";
import "./booklist.scss";

export default function BookList(props) {
  const [books, setList] = useState([]);
  const [name, setName] = useState("");
  const [authorID, setAuthorID] = useState("");
  const [genres, setGenres] = useState("");
  const token = localStorage.getItem("token");
  const [requested, updateRequested] = useState(false);

  if (!requested) {
    updateRequested(true);
    axios
      .get(`http://localhost:3000/books`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response.data);
        setList(response.data);
      })
      .catch((error) => console.error(error));
  }
  function refreshContent() {
    const userObject = {
      name: name,
      authorId: authorID,
      genres: genres,
    };
    axios
      .post("http://localhost:3000/books", userObject, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response.status);
        window.location.reload();
      })
      .catch((error) => {
        alert(error.response.data.error);
      });
  }

  function onChangeName(e) {
    setName(e.target.value);
  }
  function onChangeAuthorID(e) {
    setAuthorID(e.target.value);
  }
  function onChangeGenres() {
    setGenres(document.getElementById("genres").value.split(" "));
  }

  return (
    <div>
      {}
      <table className="BooksTable">
        <thead>
          <tr>
            <th> Name</th>
            <th> Author </th>
            <th> Genres </th>
          </tr>
        </thead>
        <tbody>
          {books
            ? books.map((row, index) => {
                return row.author ? (
                  <tr key={index}>
                    <td>
                      {" "}
                      <Link to={`/books/${row.id}`}>{row.name}</Link>
                    </td>
                    <td> {row.author}</td>
                    <td> {row.genres.join(" ")} </td>
                  </tr>
                ) : null;
              })
            : null}
        </tbody>
      </table>
      <br />
      <form onSubmit={refreshContent}>
        <fieldset>
          <legend> Add a new book: </legend>
          <label htmlFor="title" className="title-input">
            Title:{" "}
          </label>
          <input
            className="form-input"
            type="text"
            name="name"
            value={name}
            onChange={onChangeName}
          ></input>
          <br></br>
          <label htmlFor="author">AuthorId: </label>
          <input
            className="form-input"
            type="text"
            name="author"
            value={authorID}
            onChange={onChangeAuthorID}
          ></input>
          <br></br>
          <label htmlFor="genres" className="genres-input">
            Genre:{" "}
          </label>
          <input
            className="form-input"
            type="text"
            name="genre"
            id="genres"
            onChange={onChangeGenres}
          ></input>
        </fieldset>
        <Link to="/books">
          <button
            bssize="large"
            type="submit"
            className="submit-btn"
            onClick={refreshContent}
          >
            Submit
          </button>
        </Link>
      </form>
    </div>
  );
}
