import React from "react";
import axios from "axios";
import { useState } from "react";
import { Link } from "react-router-dom";
import "./authorlist.scss";
import "@fortawesome/fontawesome-free/css/all.min.css";

export default function AuthorList(props) {
  const [authors, setList] = useState([]);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const token = localStorage.getItem("token");
  const [requested, updateRequested] = useState(false);
  if (!requested) {
    updateRequested(true);
    axios
      .get(`http://localhost:3000/authors`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response.data);
        setList(response.data);
      })
      .catch((error) => console.error(error));
  }

  function onChangeFirstName(e) {
    setFirstName(e.target.value);
  }
  function onChangeLastName(e) {
    setLastName(e.target.value);
  }

  function deleteAuthor(authorId) {
    axios
      .delete("http://localhost:3000/authors/" + authorId, {
        headers: {
          authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((response) => {
        window.location.reload();
      })
      .catch((error) => console.error(error));
  }

  function refreshContent() {
    const userObject = {
      firstName: firstName,
      lastName: lastName,
    };
    axios
      .post("http://localhost:3000/authors/", userObject, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response.status);
        window.location.reload();
      })
      .catch((error) => console.error(error));
  }

  return (
    <div>
      <table className="AuthorsTable">
        <thead>
          <tr>
            <th> Author ID </th>
            <th> First Name</th>
            <th> Last Name</th>
            <th> DELETE </th>
          </tr>
        </thead>
        <tbody>
          {authors
            ? authors.map((row, index) => {
                return (
                  <tr key={index}>
                    <td> {row._id} </td>
                    <td>
                      {" "}
                      <Link to={`/authors/${row._id}`}>
                        {row.firstName}{" "}
                      </Link>{" "}
                    </td>
                    <td> {row.lastName} </td>
                    <td>
                      <button
                        bssize="large"
                        type="submit"
                        className="reg-form-btn"
                        onClick={() => deleteAuthor(row._id)}
                      >
                        <i className="fas fa-trash-alt"></i>
                      </button>
                    </td>
                  </tr>
                );
              })
            : null}
        </tbody>
      </table>
      <br />
      <form onSubmit={refreshContent}>
        <fieldset>
          <legend> Add a new author: </legend>
          <label htmlFor="firstname">First name: </label>
          <input
            className="form-input"
            type="text"
            name="firstName"
            value={firstName}
            onChange={onChangeFirstName}
          ></input>
          <br /> <br />
          <label htmlFor="lastname">Last name: </label>
          <input
            className="form-input"
            type="text"
            name="lastName"
            value={lastName}
            onChange={onChangeLastName}
          ></input>
          <br /> <br />
        </fieldset>
        <Link to="/authors">
          <button
            bssize="large"
            type="submit"
            className="submit-btn"
            onClick={refreshContent}
          >
            Submit
          </button>
        </Link>
      </form>
    </div>
  );
}
