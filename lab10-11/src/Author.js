import React, { useState } from "react";
import axios from "axios";

function Author(props) {
  const { id } = props.match.params;
  const [author, setAuthorData] = useState({});
  const token = localStorage.getItem("token");
  const [requested, updaterequested] = useState(false);

  if (!requested) {
    updaterequested(true);
    axios
      .get(`http://localhost:3000/authors/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response.data);
        setAuthorData(response.data);
      })
      .catch((error) => console.error(error));
  }

  return (
    <div className="container-author">
      <div>Author ID: {author._id}</div>
      <div>Author firstname: {author.firstName}</div>
      <div>Author lastname: {author.lastName}</div>
    </div>
  );
}
export default Author;
