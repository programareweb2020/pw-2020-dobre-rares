import React from 'react';
import './style.scss';
import Login from './Login';
import BookList from "./BookList";
import AuthorList from './AuthorList';
import Author from './Author';
import Book from './Book';
import { HashRouter, Switch, Route } from "react-router-dom";

function App() {
  return (
    <>
      <div className="app">
      <HashRouter basename="/">
        <Switch>
        <Route exact path={"/"} component={() => <h1>Index</h1>} />
        <Route path={"/login"} exact component={Login} />
        <Route path={"/books"} component={BookList} />
        <Route path={"/books/:id"} component={Book}/>
        <Route path={"/authors/:id"} component={Author}/>
        <Route path={"/authors"} component={AuthorList} />
        </Switch>
      </HashRouter>
      </div>
    </>
  );
}

export default App;
