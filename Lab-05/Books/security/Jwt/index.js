const jwt = require("jsonwebtoken");

const { ServerError } = require("../../errors");

const { validateFields } = require("../../utils");

const options = {
  issuer: process.env.JWT_ISSUER,
  subject: process.env.JWT_SUBJECT,
  audience: process.env.JWT_AUDIENCE
};

const generateToken = async payload => {
  // to be done
  // HINT: folositi functia "sign" din biblioteca jsonwebtoken
  // HINT2: seamana cu functia verify folosita mai jos ;)
  /*
     payload trebuie sa fie de forma:
     {
         userId: ,
         userRole: 
     }
    */
  try {
    const encoded = await jwt.sign(
      payload,
      process.env.JWT_SECRET_KEY,
      options
    );
    return encoded;
  } catch (err) {
    console.trace(err);
    throw new ServerError("Eroare la generarea tokenului!", 400);
  }
};

const verifyAndDecodeData = async token => {
  try {
    const decoded = await jwt.verify(
      token,
      process.env.JWT_SECRET_KEY,
      options
    );
    return decoded;
  } catch (err) {
    console.trace(err);
    throw new ServerError("Eroare la decriptarea tokenului!", 400);
  }
};

const authorizeAndExtractToken = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      throw new ServerError("Lipseste headerul de autorizare!", 403);
    }
    const token = req.headers.authorization.split(" ")[1];

    validateFields({
      jwt: {
        value: token,
        type: "jwt"
      }
    });

    const decoded = await verifyAndDecodeData(token);
    req.state = {
      decoded
    };

    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  generateToken,
  authorizeAndExtractToken
};
