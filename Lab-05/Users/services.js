const { query } = require("../data");

const { generateToken } = require("../security/Jwt");

const { ServerError } = require("../errors");

const { hash, compare } = require("../security/Password");

const addRole = async value => {
  await query("INSERT INTO roles (value) VALUES ($1)", [value]);
};

const getRoles = async () => {
  return await query("SELECT * FROM roles");
};

const getUsers = async () => {
  return await query("SELECT * FROM users");
};

const add = async (username, password, role_id) => {
  hasdpassword = await hash(password);

  await query(
    "INSERT INTO users (username, password, role_id) VALUES ($1, $2, $3)",
    [username, hasdpassword, role_id]
  );
};

const authenticate = async (username, password) => {
  const result = await query(
    `SELECT u.id, u.password, r.value as role FROM users u 
                                JOIN roles r ON r.id = u.role_id
                                WHERE u.username = $1`,
    [username]
  );
  if (result.length === 0) {
    throw new ServerError(
      `Utilizatorul cu username ${username} nu exista in sistem!`,
      400
    );
  }
  const user = result[0];
  const userId = user.id;
  const hashpassword = user.password;
  const userRole = user.role;

  const payload = {
    userId,
    userRole
  };
  if (compare(password, hashpassword)) {
    token = await generateToken(payload);
  } else {
    throw new ServerError(
      `Parola pentru utilizatorul ${username} nu este corecta!`,
      400
    );
  }
  return token;
};

module.exports = {
  add,
  addRole,
  getRoles,
  getUsers,
  authenticate
};
