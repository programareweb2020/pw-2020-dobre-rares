const {
    Books
} = require('../data');

const add = async (name, author, genres) => {
    const book = new Books({
        name,
        author,
        genres
    });
    await book.save();
};

const getAll = async () => {
    books = await Books.find({}).populate('author')

    return books.map(book => {
        return {
            name: book.name,
            author: book.author.firstName + " " + book.author.lastName,
            genres: book.genres
        }
    });
};

const getById = async (id) => {
    books = await Books.findById(id).populate('author');

    return books.map(book => {
        return {
            name: book.name,
            author: book.author.firstName + " " + book.author.lastName,
            genres: book.genres
        }
    });
};

const getByAuthorId = async (id) => {
    books = await Books.find({"author": id}).populate('author');

    return books.map(book => {
        return {
            name: book.name,
            author: book.author.firstName + " " + book.author.lastName,
            genres: book.genres
        }
    });
};

const updateById = async (id, name, authorId, genres) => {
    await Books.findByIdAndUpdate(id, { name, author, genres });
};

const deleteById = async (id) => {
    await Books.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByAuthorId,
    updateById,
    deleteById
}