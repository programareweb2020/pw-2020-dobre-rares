import React from "react";
import logo from "./logo.svg";
import "./App.css";

function Counter() {
  const [state, setState] = React.useState(0);

  const onClickIncrement = () => setState(state + 1);
  const onClickDecrement = () => setState(state - 1);
  const onClickReset = () => setState(0);

  return (
    <div>
      <h1>{state}</h1>
      <button onClick={onClickIncrement}> Increment </button>
      <button onClick={onClickDecrement}> Decrement </button>
      <button onClick={onClickReset}> Reset </button>
    </div>
  );
}

function Header(props) {
  return <h1 className="Header"> {props.name} </h1>;
}

function Options(props) {
  return <a> {props.name} </a>;
}

function Nav(props) {
  return (
    <nav className="Nav">
      {props.name}

      <Options name="Noroc"></Options>

      <Options name="Bucegi"></Options>

      <Options name="Bergenbier"></Options>
    </nav>
  );
}

function Footer(props) {
  return <footer className="Subsolu"> {props.name} </footer>;
}

function Layout(props) {
  return <div className="Layout"> {props.children} </div>;
}

function App() {
  return (
    <div>
      <Layout>
        <Header name="Mansarda"></Header>
        <Nav name="Nav"></Nav>
        <Counter></Counter>
        <Footer name="Subsolu"></Footer>
      </Layout>
    </div>
  );
}

export default App;
